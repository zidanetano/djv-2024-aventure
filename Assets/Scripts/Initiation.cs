using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Initiation : MonoBehaviour
{
    void Start()
    {
        Debug.Log("Ceci est un Start");
    }

    void Update()
    {
        Debug.Log("Ceci est un Update");
    }

    void OnEnable()
    {
        Debug.Log("Ceci est un OnEnable");
    }

    void OnDisable()
    {
        Debug.Log("Ceci est un OnDisable");
    }
}

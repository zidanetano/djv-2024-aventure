using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AccueilManager : MonoBehaviour
{
    public GameObject menuJouer;
    public GameObject menuNiveaux;

    public Button btnJouer;
    public Button btnQuitterMenuNiveaux;

    void Awake()
    {
        // Assigner la liste de niveaux dans Persistance
        Persistance.PreparerListeNiveaux(FindObjectsOfType<BtnNiveau>().Length);
    }

    void Start()
    {
        // Assigner les Listener
        btnJouer.onClick.AddListener(btnJouer_clicked);
        btnQuitterMenuNiveaux.onClick.AddListener(btnQuitterMenuNiveaux_clicked);

        // Par défaut, c'est le menu jouer qui est affiché
        btnQuitterMenuNiveaux_clicked();        
    }

    void btnJouer_clicked()
    {
        // Masquer le menuJouer
        menuJouer.SetActive(false);

        // Afficher le menu des niveaux
        menuNiveaux.SetActive(true);
    }

    void btnQuitterMenuNiveaux_clicked()
    {
        // Afficher le menuJouer
        menuJouer.SetActive(true);

        // Masquer le menu des niveaux
        menuNiveaux.SetActive(false);
    }
}

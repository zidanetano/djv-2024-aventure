using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Classe contenant toutes les fonctionnalités communes à tous
/// les personnages du jeu
/// </summary>
public class Personnage : MonoBehaviour
{
    [Header("Personnage")]
    // Points de vie
    [SerializeField] protected float pointsVieMax = 100f;
    [SerializeField] protected float pointsVie;

    // Déplacements
    [SerializeField] float vitesse;

    // Attaque
    // Dégats infligés par le personnage
    [SerializeField] float puissanceMin;
    [SerializeField] float puissanceMax;
    [SerializeField] float delaiAttaque;

    // Animations
    [SerializeField] protected bool estMort;

    // Audio
    [SerializeField] AudioClip[] acSubirDegats;
    [SerializeField] AudioClip[] acMourir;

    void Start()
    {
        
    }

    void Update()
    {
        
    }

    // Appelé lorsque le personnage subit des dégats
    public void subirDegats(float degats)
    {
        // Si je suis déjà mort, on ne fait rien
        if (estMort)
            return;

        // Calculer les pv restants
        pointsVie -= degats;

        // Vérifier si le personnage est mort
        if (pointsVie <= 0f)
        {
            pointsVie = 0f;
            mourir();
        }
    }

    public virtual void mourir()
    {
        estMort = true;

        //TODO Effet sonore
    }

    // Joue un effet sonore aléatoire d'une liste
    protected void jouerEffetSonore(AudioClip[] liste)
    {
        // TODO Assigner un AudioSource

        AudioSource _source = null;

        _source.PlayOneShot(liste[Random.Range(0, liste.Length)]);
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Orc : MonoBehaviour
{
    NavMeshAgent agent;
    Camera mainCam;

    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        mainCam = Camera.main;
    }

    void Update()
    {
        // Lorsque je fais un clic-gauche
        if (Input.GetMouseButtonDown(0))
        {
            // Trouver la position
            Vector3 position = mainCam.ScreenToWorldPoint(Input.mousePosition);

            // Corriger la position
            position.z = 0f;

            agent.SetDestination(position);

            Debug.Log($"Position du curseur: {position}");
        }
    }
}
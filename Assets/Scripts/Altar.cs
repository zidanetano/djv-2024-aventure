using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Altar : MonoBehaviour
{
    Animator animator;

    void Start()
    {
        animator = GetComponent<Animator>();
    }

    // Détecter l'arrivé du joueur
    void OnTriggerEnter2D(Collider2D collision)
    {
        //Debug.Log("Joueur à proximité");

        // Démarrer l'animation de l'autel
        animator.SetBool("joueurEnProximite", true);
    }

    // Détecter le départ du joueur
    void OnTriggerExit2D(Collider2D collision)
    {
        //Debug.Log("Joueur est parti");

        // Arrêter l'animation de l'autel
        animator.SetBool("joueurEnProximite", false);
    }
}

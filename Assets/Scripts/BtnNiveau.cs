using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class BtnNiveau : MonoBehaviour
{
    public int id;

    void Start()
    {
        Button btn = GetComponent<Button>();

        // Listener
        btn.onClick.AddListener(BtnNiveau_clicked);

        // Récupérer la disponibilité du niveau
        bool disponibilite = Persistance.GetDisponibiliteNiveau(id);

        // Rendre le bouton non-interagissable
        btn.interactable = disponibilite;
    }

    void BtnNiveau_clicked()
    {
        Persistance.idNiveau = id;

        // Charger la scène de jeu
        SceneManager.LoadScene("Jeu");
    }
}

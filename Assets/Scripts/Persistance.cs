using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Persistance
{
    public static int idNiveau;

    static bool[] disponibiliteNiveaux;

    public static void PreparerListeNiveaux(int qte)
    {
        // Si la liste existe déjà, on ne la réinitialise pas
        if (disponibiliteNiveaux != null)
            return;

        Debug.Log($"PreparerListeNiveaux qte: {qte}");

        disponibiliteNiveaux = new bool[qte];

        // Le niveau 1 est disponible en tout temps
        disponibiliteNiveaux[0] = true;
    }

    public static bool GetDisponibiliteNiveau(int idNiveau)
    {
        return disponibiliteNiveaux[idNiveau - 1];
    }

    public static void SetNiveauComplete(int idNiveau)
    {
        // Rendre disponible le niveau suivant
        if (idNiveau < disponibiliteNiveaux.Length)        
            disponibiliteNiveaux[idNiveau] = true;

        Debug.Log($"Le niveau {idNiveau} a été complété, on débloque le suivant");
    }
}

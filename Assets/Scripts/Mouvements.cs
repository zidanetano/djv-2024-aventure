using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mouvements : MonoBehaviour
{
    public float speed = 5f;

    public Animator animator;

    float horizontal, vertical;

    Vector2 direction;

    Vector3 defaultScale;
    Vector3 tempScale;

    Rigidbody2D rb;

    public bool estEtourdit;

    void Start()
    {
        defaultScale = transform.localScale;
        tempScale = defaultScale;

        rb = GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        // Assigner les valeurs d'input d'axes
        horizontal = Input.GetAxisRaw("Horizontal");
        vertical = Input.GetAxisRaw("Vertical");

        direction.x = horizontal;
        direction.y = vertical;

        // Normaliser le vecteur de déplacement pour ne pas accéler lorsque
        // je me déplace en angle de 45 degrés
        direction.Normalize();          

        // Aviser l'animator du déplacement
        animator.SetFloat("velocite", direction.magnitude);

        // Vérifier la direction du mouvement
        if (horizontal < 0f)
            tempScale.x = defaultScale.x * -1f;
        else if (horizontal > 0f)
            tempScale.x = defaultScale.x;

        // Assigner le scale
        transform.localScale = tempScale;

        // Attaquer
        if (Input.GetMouseButtonDown(0))
        {
            animator.SetTrigger("attack");
        }
    }

    void FixedUpdate()
    {
        // Déplacement du personnage
        if (!estEtourdit)
            rb.velocity = direction * speed * Time.fixedDeltaTime;
    }
}

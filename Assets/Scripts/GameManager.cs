using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public GameObject prefabPiece;

    // Nombre de pièce EN TOUT
    int piecesTotal;

    int piecesRecoltees = 0;

    AudioSource source;

    int idNiveau;

    // Start is called before the first frame update
    void Start()
    {
        // Instantier la bonne quantité de pièces
        idNiveau = Persistance.idNiveau;

        for (int i = 0; i < idNiveau * 2; i++)
        {
            // Déterminer la position
            Vector2 position = new Vector2(
                Random.Range(-2.5f, 3f),
                Random.Range(-2.5f, 2.5f));

            // Instantier
            Instantiate(prefabPiece, position, Quaternion.identity);
        }

        // Trouver dynamiquement le nombre de pièces dans ma scène
        piecesTotal = FindObjectsOfType<PieceMonnaie>().Length;

        // Assigner l'audio source
        source = GetComponent<AudioSource>();

        Debug.Log($"Chargement du niveau {Persistance.idNiveau}");

    }    

    // Appelée lorsqu'une pièce est ramassée
    public void PieceRecoltee()
    {
        Debug.Log("Pièce récoltée");

        // Compter les pièces ramassées
        piecesRecoltees++;

        // Effet sonore
        source.Play();

        // Fin de partie?
        if (piecesRecoltees == piecesTotal)
        {
            Debug.Log($"GAME OVER! Durée de partie: {Time.time}");

            // Empêcher les mouvements du personnage
            FindObjectOfType<Mouvements>().enabled = false;

            // Aviser la persistance de la réussite du niveau
            Persistance.SetNiveauComplete(idNiveau);

            // Charger la scène d'accueil
            SceneManager.LoadScene("Accueil");
        }
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class GuiTesting : MonoBehaviour
{
    public Toggle tglRTX;
    public Button btnDemarrer;
    public TMP_Dropdown ddDifficulte;
    public TMP_InputField ipfNom;
    public Slider sldVolume;

    void Start()
    {
        // Assigner les Event Listener
        btnDemarrer.onClick.AddListener(btnDemarrer_clicked);
        tglRTX.onValueChanged.AddListener(tglRTX_checked);
        sldVolume.onValueChanged.AddListener(sldVolume_onValueChanged);
        ipfNom.onEndEdit.AddListener(ipfNom_onValueChanged);
    }

    void btnDemarrer_clicked()
    {
        Debug.Log("Le joueur a appuyé sur Démarrer");
    }

    void tglRTX_checked(bool value)
    {
        Debug.Log($"Le RTX est maintenant {value}");
    }

    void sldVolume_onValueChanged(float valeur)
    {
        Debug.Log($"Le volume est maintenant à: {valeur}");
    }

    void ipfNom_onValueChanged(string valeur)
    {
        Debug.Log($"Le nom est maintenant: {valeur}");
    }
}

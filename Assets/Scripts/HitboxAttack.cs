using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitboxAttack : MonoBehaviour
{
    Collider2D hitbox;

    void Start()
    {
        // Assigner le collider2D
        hitbox = GetComponent<Collider2D>();

        // Désactiver au départ
        ToggleHitbox(false);
    }

    // Activer/Désactiver la hitbox
    public void ToggleHitbox(bool value)
    {
        hitbox.enabled = value;
    }

    // Impact avec un objet?
    void OnTriggerEnter2D(Collider2D collision)
    {
        Debug.Log($"Impact avec {collision.name}");

        // Vérifier pour trouver un HealthEntity
        if (collision.TryGetComponent(out HealthEntity healthEntity))
        {
            healthEntity.subirDegats(15f);
        }
    }
}

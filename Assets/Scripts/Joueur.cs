using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Joueur : Personnage
{
    [Header("Joueur")]
    [SerializeField] GameObject Toki;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public override void mourir()
    {
        base.mourir();

        // TODO: Animation de mort du joueur

        // Appeler la résurection
        Invoke("ressusciter", 3f);
    }

    void ressusciter()
    {
        // TODO Téléporter le joueur au village

        // Remettre sa vie à pleine capacité
        pointsVie = pointsVieMax;

        // Retirer le statut de estMort
        estMort = false;

        // TODO Aviser Toki de ma résurrection
    }
}

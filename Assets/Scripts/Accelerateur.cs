using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Accelerateur : MonoBehaviour
{
    void OnTriggerEnter2D(Collider2D collision)
    {
        // S'agit-il du personnage?
        if (collision.TryGetComponent(out Mouvements mouvements))
        {
            mouvements.estEtourdit = true;
        }
    }

    void OnTriggerExit2D(Collider2D collision)
    {
        // S'agit-il du personnage?
        if (collision.TryGetComponent(out Mouvements mouvements))
        {
            StartCoroutine(Sortir(mouvements));
        }
    }

    // Coroutine pour sortir de l'état d'étourdit
    IEnumerator Sortir(Mouvements mouvements)
    {
        // Prend une pause
        yield return new WaitForSeconds(0.5f);

        // Sortir de l'état d'étourdissement
        mouvements.estEtourdit = false;
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAttack : MonoBehaviour
{
    public HitboxAttack hitbox;

    public void AttaqueDebut()
    {

        // Activer la hitbox
        hitbox.ToggleHitbox(true);
    }

    public void AttaqueFin()
    {
        // Désactiver la hitbox
        hitbox.ToggleHitbox(false);
    }
}
